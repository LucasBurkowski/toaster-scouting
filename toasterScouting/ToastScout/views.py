from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.urls import reverse
from ToastScout.models import TeamMatchDataForm, TeamMatchData
from ToastScout.tba_data import getEvent
import traceback

# Create your views here.
def index(request):
    return render(request, 'ToastScout/home.html')
    
def event_setup(request):
    if "GET" == request.method:
        return render(request, 'ToastScout/event_setup.html')
    try:
        event_key = request.POST["event_key"]
        getEvent(event_key)
        return HttpResponseRedirect(reverse("index"))
    except Exception as e:
        messages.error(request,"Unable to import data. "+repr(e))
        return HttpResponseRedirect(request.path)
    
    
def upload_csv(request):
    data = {}
    if "GET" == request.method:
	    return render(request, 'ToastScout/upload.html', {'Title': 'Upload'})
    # if not GET, then proceed
    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            #messages.error(request,'File is not CSV type')
            return HttpResponseRedirect(reverse("upload"))
    #if file is too large, return
        if csv_file.multiple_chunks():
            #messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return HttpResponseRedirect(reverse("upload"))
    
        file_data = csv_file.read().decode("utf-8")		
    
        lines = file_data.split("\n")
    	#loop over the lines and save them in db. If error , store as string and then display
        for line in lines:						
            fields = line.split(",")
            data_dict = {}
            
            if len(fields) == 44:
                for i in range(13, 27):
                    if "|" in fields[i]:
                        numbers = fields[i].split("|")
                        greatest = numbers[0]
                        for number in numbers:
                            if number > greatest:
                                greatest = number
                        fields[i] = greatest
        		
                for i in range(len(fields)):
                    if fields[i] is '':
                        fields[i] = '0'
                        
                data_dict["fileName"] = fields[0]
                data_dict["autonAlliance2BallTrenchPickup"] = fields[1]
                data_dict["autonAlliance3BallTrenchPickup"] = fields[2]
                data_dict["autonHighGoalPowerCells"] = fields[3]
                data_dict["autonLowGoalPowerCells"] = fields[4]
                data_dict["climbPositionMiddle"] = fields[5]
                data_dict["climbPositionRight"] = fields[6]
                data_dict["climbPositionLeft"] = fields[7]
                data_dict["climbed"] = fields[8]
                data_dict["crossedInitiationLine"] = fields[9]
                data_dict["defense"] = fields[10]
                data_dict["didPositionControl"] = fields[11]
                data_dict["didRotationControl"] = fields[12]
                data_dict["highGoalCycles0"] = fields[13]
                data_dict["highGoalCycles1"] = fields[14]
                data_dict["highGoalCycles2"] = fields[15]
                data_dict["highGoalCycles3"] = fields[16]
                data_dict["highGoalCycles4"] = fields[17]
                data_dict["loadCycles0"] = fields[18]
                data_dict["loadCycles1"] = fields[19]
                data_dict["loadCycles2"] = fields[20]
                data_dict["loadCycles3"] = fields[21]
                data_dict["loadCycles4"] = fields[22]
                data_dict["lowGoalCycles0"] = fields[23]
                data_dict["lowGoalCycles1"] = fields[24]
                data_dict["lowGoalCycles2"] = fields[25]
                data_dict["lowGoalCycles3"] = fields[26]
                data_dict["lowGoalCycles4"] = fields[27]
                data_dict["match0"] = fields[28]
                data_dict["match1"] = fields[29]
                data_dict["match2"] = fields[30]
                data_dict["autonOpponent2BallTrench"] = fields[31]
                data_dict["autonOpponent3BallTrench"] = fields[32]
                data_dict["pickUp"] = fields[33]
                data_dict["playedDefense"] = fields[34]
                data_dict["positionControl"] = fields[35]
                data_dict["fieldPosition"] = fields[36]
                data_dict["rendezvous"] = fields[37]
                data_dict["rotationControl"] = fields[38]
                data_dict["speed"] = fields[39]
                data_dict["team0"] = fields[40]
                data_dict["team1"] = fields[41]
                data_dict["team2"] = fields[42]
                data_dict["team3"] = fields[43]
        		
                try:
                    form = TeamMatchDataForm(data_dict)
                    if form.is_valid():
                        matchData = TeamMatchData(
                            rawFile = csv_file,
                            teamNumber = form.getTeamNumber(),
                            matchNumber = form.getMatchNumber(),
                            teamStartingPosition = form.cleaned_data["fieldPosition"],
                            crossedInitiationLine = form.cleaned_data["crossedInitiationLine"],
                            autonHighGoalPowerCells = form.cleaned_data["autonHighGoalPowerCells"],
                            autonLowGoalPowerCells = form.cleaned_data["autonLowGoalPowerCells"],
                            autonAlliance3BallTrenchPickup = form.cleaned_data["autonAlliance3BallTrenchPickup"],
                            autonAlliance2BallTrenchPickup = form.cleaned_data["autonAlliance2BallTrenchPickup"],
                            autonRZPickup = form.cleaned_data["pickUp"],
                            autonOpponet3BallTrenchPickup = form.cleaned_data["autonOpponent3BallTrench"],
                            autonOpponet2BallTrenchPickup = form.cleaned_data["autonOpponent2BallTrench"],
                            teleopNumberOfLoadCycles = form.getLoadCycles(),
                            teleopNumberOfLoadedPowerCells = form.getNumberOfLoadedPowerCells(),
                            teleopNumberOfHighCycles = form.getHighGoalCycles(),
                            teleopNumberOfHighPowerCells = form.getTeleopNumberOfHighPowerCells(),
                            teleopNumberofLowcycles = form.getLowGoalCycles(),
                            teleopNumberofLowpowercells = form.getTeleopNumberOfLowPowerCells(),
                            playedDefense = form.cleaned_data["playedDefense"],
                            climbed = form.cleaned_data["climbed"], 
                            climbPositionLeft = form.cleaned_data["climbPositionLeft"],
                            climbPositionMiddle = form.cleaned_data["climbPositionMiddle"],
                            climbPositionRight = form.cleaned_data["climbPositionRight"],
                            speed = form.cleaned_data["speed"],
                            defense = form.cleaned_data["defense"],
                            rotationControlRating = form.cleaned_data["rotationControl"],
                            positionControlRating = form.cleaned_data["positionControl"],
                            rotationControl = form.cleaned_data["didRotationControl"],
                            positionControl = form.cleaned_data["didPositionControl"])
                        matchData.save()
                except Exception as e:
                    print(repr(e))					
                    pass
    
    except Exception as e:
        print("Unable to upload file. "+repr(e))
        messages.error(request,"Unable to upload file. "+repr(e))
        traceback.print_exc()
    
    return HttpResponseRedirect(reverse("upload"))