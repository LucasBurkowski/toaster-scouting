from django.contrib import admin
from ToastScout.models import Team, Match, Event, TeamMatchData

# Register your models here.
admin.site.register(Team)
admin.site.register(Match)
admin.site.register(Event)
admin.site.register(TeamMatchData)