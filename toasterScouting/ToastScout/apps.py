from django.apps import AppConfig


class ToastscoutConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ToastScout'
