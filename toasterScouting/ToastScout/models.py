from django.db import models
from django import forms

class Event(models.Model):
    name = models.CharField(max_length=100)
    key = models.CharField(max_length=100)
    date = models.DateTimeField()
    
class Match(models.Model):
    RED = 'R'
    BLUE = 'B'
    TIE = 'T'
    number = models.IntegerField()
    redScore = models.IntegerField()
    blueScore = models.IntegerField()
    winningAlliance = models.CharField(max_length=1, default=TIE)
    
class Team(models.Model):
    teamNumber = models.IntegerField()
    totalCyclesMAX = models.FloatField(default=0.0)
    TotalCyclesAVG = models.FloatField(default=0.0)
    AVGAutonHGPowerCellsMAX = models.FloatField(default=0.0)
    AutonLowGoalPowerCellsMAX = models.FloatField(default=0.0)
    TeleopNumLOADcyclesMAX = models.FloatField(default=0.0)
    TeleopNumLOADEDpowercellsMAX = models.FloatField(default=0.0)
    TeleopNumHIGHcyclesMAX = models.FloatField(default=0.0)
    TeleopNumHIGHpowercellsMAX = models.FloatField(default=0.0)
    TeleopNumLOWcyclesMAX = models.FloatField(default=0.0)
    TeleopNumLOWpowercellsMAX = models.FloatField(default=0.0)
    InitiationLine = models.FloatField(default=0.0)
    AutonHighGoalPowerCells = models.FloatField(default=0.0)
    AutonLowGoalPowerCells = models.FloatField(default=0.0)
    AutonOur3BallTrenchPickup = models.FloatField(default=0.0)
    AutonOur2BallTrenchPickup = models.FloatField(default=0.0)
    AutonRZPickup = models.FloatField(default=0.0)
    AutonOpponent3BallTrenchPickup = models.FloatField(default=0.0)
    AutonOpponent2BallTrenchPickup = models.FloatField(default=0.0)
    TeleopNumberofLOADcycles = models.FloatField(default=0.0)
    TeleopNumofLOADEDpowercells = models.FloatField(default=0.0)
    TeleopNumofHIGHcycles = models.FloatField(default=0.0)
    TeleopNumofHIGHpowercells = models.FloatField(default=0.0)
    TeleopNumofLOWcycles = models.FloatField(default=0.0)
    TeleopNumberofLOWpowercells = models.FloatField(default=0.0)
    PlayedDefense = models.FloatField(default=0.0)
    Climbed = models.FloatField(default=0.0)
    ClimbPositionLeft = models.FloatField(default=0.0)
    ClimbPositionMiddle = models.FloatField(default=0.0)
    ClimbPositionRight = models.FloatField(default=0.0)
    SpeedRating = models.FloatField(default=0.0)
    Defense = models.FloatField(default=0.0)
    RotationControlRating = models.FloatField(default=0.0)
    PositionControlRating = models.FloatField(default=0.0)
    RotationControl = models.FloatField(default=0.0)
    PositionControl = models.FloatField(default=0.0)
    AVGNumPowerCellsperloadcycle = models.FloatField(default=0.0)
    AVGNumPowerCellsperHIGHcycles = models.FloatField(default=0.0)
    AVGNumofpowercellsperLOWcycles = models.FloatField(default=0.0)
    AutonHighGoalPowerCellsMAX = models.FloatField(default=0.0)

class TeamMatchData(models.Model):
    rawFile = models.FileField(upload_to = "rawdata")
    teamNumber = models.IntegerField()
    matchNumber = models.IntegerField()
    teamStartingPosition = models.CharField(max_length=2)
    crossedInitiationLine = models.IntegerField()
    autonHighGoalPowerCells = models.IntegerField()
    autonLowGoalPowerCells = models.IntegerField()
    autonAlliance3BallTrenchPickup = models.IntegerField() #called Allaince 3 ball trench on sheets
    autonAlliance2BallTrenchPickup = models.IntegerField() # also called Allaince 2 ball trench on sheets
    autonRZPickup = models.IntegerField()
    autonOpponet3BallTrenchPickup = models.IntegerField()
    autonOpponet2BallTrenchPickup = models.IntegerField()
    teleopNumberOfLoadCycles = models.IntegerField()
    teleopNumberOfLoadedPowerCells = models.IntegerField()
    teleopNumberOfHighCycles = models.IntegerField()
    teleopNumberOfHighPowerCells = models.IntegerField()
    teleopNumberofLowcycles	= models.IntegerField()
    teleopNumberofLowpowercells	= models.IntegerField()
    playedDefense = models.IntegerField()
    climbed = models.IntegerField()
    climbPositionLeft = models.IntegerField()
    climbPositionMiddle	= models.IntegerField()
    climbPositionRight = models.IntegerField()
    speed = models.IntegerField()
    defense = models.IntegerField()
    rotationControlRating = models.IntegerField()
    positionControlRating = models.IntegerField()
    rotationControl = models.IntegerField()
    positionControl = models.IntegerField()
    
class TeamMatchDataForm(forms.Form):
    fileName = forms.IntegerField()
    autonAlliance2BallTrenchPickup = forms.IntegerField() # also called Allaince 2 ball trench on sheets
    autonAlliance3BallTrenchPickup = forms.IntegerField() #called Allaince 3 ball trench on sheets #the dictionary doesn't say pickup anymore
    autonHighGoalPowerCells = forms.IntegerField()
    autonLowGoalPowerCells = forms.IntegerField()
    climbPositionMiddle = forms.IntegerField() 
    climbPositionRight = forms.IntegerField()
    climbPositionLeft = forms.IntegerField() 
    climbed = forms.IntegerField()
    crossedInitiationLine = forms.IntegerField()
    defense = forms.IntegerField()
    didPositionControl = forms.IntegerField()
    didRotationControl = forms.IntegerField()
    highGoalCycles0 = forms.IntegerField()
    highGoalCycles1 = forms.IntegerField()
    highGoalCycles2 = forms.IntegerField()
    highGoalCycles3 = forms.IntegerField()
    highGoalCycles4 = forms.IntegerField()
    loadCycles0 = forms.IntegerField()
    loadCycles1 = forms.IntegerField()
    loadCycles2 = forms.IntegerField()
    loadCycles3 = forms.IntegerField()
    loadCycles4 = forms.IntegerField()
    lowGoalCycles0 = forms.IntegerField()
    lowGoalCycles1 = forms.IntegerField()
    lowGoalCycles2 = forms.IntegerField()
    lowGoalCycles3 = forms.IntegerField()
    lowGoalCycles4 = forms.IntegerField()
    match0 = forms.IntegerField()
    match1 = forms.IntegerField()
    match2 = forms.IntegerField()
    autonOpponent2BallTrench  = forms.IntegerField()
    autonOpponent3BallTrench = forms.IntegerField()
    pickUp = forms.IntegerField()
    playedDefense = forms.IntegerField()
    positionControl = forms.IntegerField()
    fieldPosition = forms.CharField() # this one uses letters too
    rendezvous = forms.IntegerField() 
    rotationControl = forms.IntegerField() 
    speed = forms.IntegerField() 
    team0 = forms.IntegerField() 
    team1 = forms.IntegerField() 
    team2 = forms.IntegerField() 
    team3 = forms.IntegerField() 
    
    def getTeamNumber(self):
       return (self.cleaned_data["team0"] * 1000 + self.cleaned_data["team1"] * 100 + self.cleaned_data["team2"] * 10 + self.cleaned_data["team3"])
          
    def getMatchNumber(self):
        return (self.cleaned_data["match0"] * 100 + self.cleaned_data["match1"] * 10 + self.cleaned_data["match2"])
            
    def getTeleopNumberOfHighPowerCells(self):
        return (self.cleaned_data["highGoalCycles0"] + self.cleaned_data["highGoalCycles1"]*2 + self.cleaned_data["highGoalCycles2"]*3 + self.cleaned_data["highGoalCycles3"]*4 + self.cleaned_data["highGoalCycles4"]*5)
    
    def getHighGoalCycles(self):
        return (self.cleaned_data["highGoalCycles0"] + self.cleaned_data["highGoalCycles1"] + self.cleaned_data["highGoalCycles2"] + self.cleaned_data["highGoalCycles3"] + self.cleaned_data["highGoalCycles4"])
       
    def getNumberOfLoadedPowerCells(self):
        return  (self.cleaned_data["loadCycles0"] + self.cleaned_data["loadCycles1"] *2 + self.cleaned_data["loadCycles2"]*3 + self.cleaned_data["loadCycles3"]*4 + self.cleaned_data["loadCycles4"]*5)
        
    def getLoadCycles(self):
        return  (self.cleaned_data["loadCycles0"] + self.cleaned_data["loadCycles1"] + self.cleaned_data["loadCycles2"] + self.cleaned_data["loadCycles3"] + self.cleaned_data["loadCycles4"])
    
    def getLowGoalCycles(self):
        return (self.cleaned_data["lowGoalCycles0"] + self.cleaned_data["lowGoalCycles1"] + self.cleaned_data["lowGoalCycles2"] + self.cleaned_data["lowGoalCycles3"] + self.cleaned_data["lowGoalCycles4"])

    def getTeleopNumberOfLowPowerCells(self):
        return (self.cleaned_data["lowGoalCycles0"] + self.cleaned_data["lowGoalCycles1"]*2 + self.cleaned_data["lowGoalCycles2"]*3 + self.cleaned_data["lowGoalCycles3"]*4 + self.cleaned_data["lowGoalCycles4"]*5)
        
    