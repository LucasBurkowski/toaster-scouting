from ToastScout.models import Event, Team
import tbapy

tba = tbapy.TBA("kCtXBSEh3RdjLHuPlVFO9nAzoZ6PEC1kqwbDojUF2Gl5nWXF6xwPEtk1XUgvD06x")

def getEvent(event_key):
    event = tba.event(event_key)
    if Event.objects.filter(key = event.key).exists() == False:
        name = event.name
        date = event.start_date
        key = event.key
        Event.objects.create(name = name, date = date, key = key)
    getTeams(event_key)
    
def getTeams(event_key):
    event = Event.objects.get(key = event_key)
    teams = tba.event_teams(event.key)
    for team in teams:
        if Team.objects.filter(teamNumber = team.team_number).exists() == False:
            Team.objects.create(teamNumber = team.team_number)